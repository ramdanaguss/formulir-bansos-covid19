# formulir-bansos-covid19 / covid19-social-assistance-form

Untuk mendapatkan hasil "Success" dan "Internal Server Error" mohon isi form sebanyak 5 - 10 kali karena proses pengiriman data tidak benar - benar mengirimkan data ke server /backend melainkan menggunakan simulasi menggunakan composable dengan metode random number sehingga mendapat kesempatan "Success" 50% dan mendapat "Internal Server Error" sebanyak 50%

Jika anda melihat git history dan melihat tanggal commitnya, aplikasi ini dibuat dalam waktu sekitar 2 jam dan mungkin anda menemukan urutan commit yang sedikit aneh. Hal tersebut karena kesalahan saya ditengah pengembangan yang lupa tidak melakukan version control dalam membuat aplikasi ini sehingga saya memutuskan untuk membereskan aplikasi ini tanpa version control terlebih dahulu, lalu membuat project baru dengan menyalin aplikasi sebelumnya dan melakukan version control.

To get the results "Success" and "Internal Server Error", please fill out the form 5 to 10 times. This is because the data submission process does not actually send the data to the server/backend, but rather uses a simulation using composable with a random number method, so there is a 50% chance of getting "Success" and a 50% chance of getting "Internal Server Error"

If you look at the git history and see the commit dates, this application was created in about 2 hours and you may find the commit order a bit strange. This is because of a mistake I made in the middle of development, when I forgot to do version control when creating this application. So I decided to finish the application without version control first, then create a new project by copying the previous application and doing version control.

## Pengaturan IDE yang Direkomendasikan / Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).

## Pengaturan Proyek / Project Setup

```sh
npm install
```

### Kompilasi dan Hot-Reload untuk Pengembangan / Compile and Hot-Reload for Development

```sh
npm run dev
```

### Kompilasi dan Minifikasi untuk Produksi / Compile and Minify for Production

```sh
npm run build
```
