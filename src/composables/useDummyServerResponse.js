import { ref } from 'vue'

const timer = ref(null)

export function useDummyServerResponse() {
  return new Promise((resolve, reject) => {
    const randomNumber = Math.random()

    if (timer.value) clearTimeout(timer.value)

    timer.value = setTimeout(() => {
      if (randomNumber < 0.5) {
        reject('Interval Server Error')
      }

      resolve('Success')
    }, 1500)
  })
}
