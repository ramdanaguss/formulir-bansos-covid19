import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

import { getData } from '../helper/getData'

export const useIndonesiaPlacesStore = defineStore('indonesiaPlaces', () => {
  const provinces = ref([])
  const province = ref({})
  const regencies = ref([])
  const regency = ref({})
  const districts = ref([])
  const district = ref({})
  const villages = ref([])
  const village = ref({})

  const provinceNames = computed(() => provinces.value.map((province) => province.name))
  const regencyNames = computed(() => regencies.value.map((regency) => regency.name))
  const districtNames = computed(() => districts.value.map((district) => district.name))
  const villageNames = computed(() => villages.value.map((village) => village.name))

  async function setProvincies() {
    try {
      provinces.value = await getData(
        'https://www.emsifa.com/api-wilayah-indonesia/api/provinces.json'
      )
    } catch (err) {
      console.error(err)
    }
  }

  async function setRegencies(provinceId) {
    try {
      regencies.value = await getData(
        `https://www.emsifa.com/api-wilayah-indonesia/api/regencies/${provinceId}.json`
      )
    } catch (err) {
      console.error(err)
    }
  }

  async function setDistricts(regencyId) {
    try {
      districts.value = await getData(
        `https://www.emsifa.com/api-wilayah-indonesia/api/districts/${regencyId}.json`
      )
    } catch (err) {
      console.error(err)
    }
  }

  async function setVillages(districtId) {
    try {
      villages.value = await getData(
        `https://www.emsifa.com/api-wilayah-indonesia/api/villages/${districtId}.json`
      )
    } catch (err) {
      console.error(err)
    }
  }

  async function setProvince(userProvince) {
    try {
      province.value = provinces.value.find(
        (province) =>
          province.name.includes(userProvince.trim()) ||
          province.name.includes(userProvince.trim().toUpperCase())
      )

      if (!province.value) return

      await setRegencies(province.value.id)
    } catch (err) {
      console.error(err)
    }
  }

  async function setRegency(userRegency) {
    try {
      regency.value = regencies.value.find(
        (regency) =>
          regency.name.includes(userRegency.trim()) ||
          regency.name.includes(userRegency.trim().toUpperCase())
      )

      if (!regency.value) return

      await setDistricts(regency.value.id)
    } catch (err) {
      console.error(err)
    }
  }

  async function setDistrict(userDistrict) {
    try {
      district.value = districts.value.find(
        (district) =>
          district.name.includes(userDistrict.trim()) ||
          district.name.includes(userDistrict.trim().toUpperCase())
      )

      if (!district.value) return

      await setVillages(district.value.id)
    } catch (err) {
      console.error(err)
    }
  }

  function setVillage(userVillage) {
    try {
      village.value = districts.value.find(
        (village) =>
          village.name.includes(userVillage.trim()) ||
          village.name.includes(userVillage.trim().toUpperCase())
      )
    } catch (err) {
      console.error(err)
    }
  }

  return {
    provinces,
    provinceNames,
    province,
    setProvincies,
    setProvince,
    regencies,
    regencyNames,
    regency,
    setRegencies,
    setRegency,
    districts,
    districtNames,
    district,
    setDistricts,
    setDistrict,
    villages,
    villageNames,
    village,
    setVillages,
    setVillage
  }
})
