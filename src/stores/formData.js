import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useFormData = defineStore(
  'formData',
  () => {
    const formData = ref({})

    function setFormData(data) {
      formData.value = data
    }

    return { formData, setFormData }
  },
  {
    persist: true
  }
)
