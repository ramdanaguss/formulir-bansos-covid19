import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      component: () => import('../pages/LandingPage.vue')
    },
    {
      path: '/form',
      component: () => import('../pages/FormPage.vue')
    }
  ]
})

export default router
