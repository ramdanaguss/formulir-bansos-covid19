import { defineRule, configure } from 'vee-validate'
import { required, numeric, max, ext, size, one_of } from '@vee-validate/rules'

const validator = {
  install() {
    defineRule('required', required)
    defineRule('numeric', numeric)
    defineRule('max', max)
    defineRule('ext', ext)
    defineRule('size', size)
    defineRule('one_of', one_of)

    defineRule('more_or_equal', (value, [limit]) => {
      if (value < limit) {
        return false
      }

      return true
    })

    configure({
      generateMessage({ field, rule }) {
        switch (rule.name) {
          case 'required':
            return `${field} harus diisi`
          case 'numeric':
            return `${field} harus berupa angka`
          case 'max':
            return `${field} tidak boleh lebih dari ${rule.params} character`
          case 'ext':
            return `Berkas ${field} harus ${rule.params}`
          case 'size':
            return `Berkas ${field} tidak boleh lebih dari ${rule.params / 1024}MB`
          case 'one_of':
            return `Isi sesuai pilihan yang tersedia`
          case 'more_or_equal':
            return `${field} harus lebih dari atau sama dengan ${rule.params}`
        }
      }
    })
  }
}

export default validator
