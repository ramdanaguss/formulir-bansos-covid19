import { createApp } from 'vue'
import { createPinia } from 'pinia'
import { Vue3Mq } from 'vue3-mq'

import App from './App.vue'
import router from './router'
import validator from './validator/validator'

import './assets/styles/main.css'

const app = createApp(App)
const pinia = createPinia()

app.use(validator)
app.use(pinia)
app.use(router)
app.use(Vue3Mq, {
  preset: 'tailwind'
})

app.mount('#app')
