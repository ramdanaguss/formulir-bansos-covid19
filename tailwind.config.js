/* eslint-disable no-undef */
/** @type {import('tailwindcss').Config} */

const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  content: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    screens: {
      xxs: '360px',
      xs: '390px',
      ...defaultTheme.screens
    },
    extend: {
      fontFamily: {
        sans: ['DM Sans', ...defaultTheme.fontFamily.sans]
      },
      colors: {
        primary: '#4A3AFF',
        'primary-tint-1': '#edebff',
        black: '#170F49',
        'grey-1': '#f2f3f8',
        'grey-2': '#EFF0F6',
        'grey-3': '#6F6C90',
        red: '#FF3A5D'
      }
    }
  },
  plugins: [require('tailwind-scrollbar')({ nocompatible: true })]
}
